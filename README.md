**This is a plugin for your browser**

Please read the appropriate guides on how to attach plugins to your browser of choice...

Chromium based browsers (chrome, opera, brave, etc...): https://www.cnet.com/tech/services-and-software/how-to-install-chrome-extensions-manually/

Mozilla based browsers (firefox, tor, librewolf, etc...): https://support.mozilla.org/en-US/questions/1207502

*Currently tested on most chrome forks and mozilla based forks as well*

---
0